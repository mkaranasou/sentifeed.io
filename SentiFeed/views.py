from django.shortcuts import render

# Create your views here.
from django.shortcuts import render_to_response


def home(request):
    return render_to_response('index.html')

def login(request):
    return render_to_response('login.html')

def register(request):
    return render_to_response('register.html')


def about(request):
    return render_to_response('about.html')