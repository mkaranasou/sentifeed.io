from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.auth.views import login
from django.contrib.auth.views import logout

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'SentiFeedWeb.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'SentiFeed.views.home', name='home'),
    url(r'^login/$', 'SentiFeed.views.login', name='login'),
    url(r'^register/$', 'SentiFeed.views.register', name='register'),
    url(r'^about/$', 'SentiFeed.views.about', name='about'),
    # url(
    #     regex=r'^login/$',
    #     view=login,
    #     kwargs={'template_name': 'login.html'},
    #     name='login'
    # ),
    # url(
    #     regex=r'^logout/$',
    #     view=logout,
    #     kwargs={'next_page': '/'},
    #     name='logout'
    # ),
    url(r'', include('SentiFeedWeb.urls')),
)
